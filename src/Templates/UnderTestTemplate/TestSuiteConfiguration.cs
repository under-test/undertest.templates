using System;
using CommandLine;
using UnderTest.Configuration;

namespace UnderTestTemplate
{
  internal class TestSuiteConfiguration : UnderTestCommandLineOptions
  {
    // and example config value is defined below
    // our config values we want to be able to set with increasing priority:
    //   1. - command line parameter
    //   2. - environment variable
    //   3. - default value
    //
    // to define using these priorities, use the template below.

    // Example:
    // [Option('e', "ExampleValue")]
    // public string ExampleValue { get; set; } = Environment.GetEnvironmentVariable("TestSuiteName.ExampleValue") ?? "this is the default value to use when the env var `SuiteName.ExampleValue` is not set and `-e` is not passed through the commandline";
  }
}
