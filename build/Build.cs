using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Nuke.Common;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.NuGet;
using Nuke.Common.Tools.Slack;
using Nuke.Common.Utilities;
using Nuke.Common.Utilities.Collections;

using static Nuke.Common.ChangeLog.ChangelogTasks;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.IO.PathConstruction;
using static Nuke.Common.Tools.Git.GitTasks;
using static Nuke.Common.Tools.NuGet.NuGetTasks;
using static Nuke.Common.Tools.Slack.SlackTasks;

[ExcludeFromCodeCoverage]
public class Build : NukeBuild
{
  [SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:ElementsMustAppearInTheCorrectOrder", Justification = "Reviewed. Suppression is OK here.")]
  public static int Main()
  {
    return Execute<Build>(x => x.Pack);
  }

  [Parameter("Api Key for Nuget.org when pushing our nuget package")]
  public readonly string NugetOrgApiKey;

  public AbsolutePath SourceDirectory => RootDirectory / "src";

  public AbsolutePath NuspecFile => SourceDirectory / "UnderTest.Templates.nuspec";

  public AbsolutePath ArtifactsDirectory => RootDirectory / "artifacts";

  [Parameter("Slack webhook")]
  public readonly string SlackWebhook;

  string ChangelogFile => RootDirectory / "CHANGELOG.md";

  [PathExecutable]
  readonly Tool Dotnet;

  IEnumerable<string> ChangelogSectionNotes => ExtractChangelogSectionNotes(ChangelogFile);

  public Target Clean =>
    _ =>
      _.Executes(() =>
      {
        foreach (var directory in GlobDirectories(SourceDirectory, "**/bin", "**/obj"))
        {
          DeleteDirectory(directory);
        }

        EnsureCleanDirectory(ArtifactsDirectory);
      });

  public Target Pack =>
    _ => _
      .DependsOn(Clean)
      .Executes(() =>
      {
        NuGetPack(x => x
          .SetBuild(false)
          .SetOutputDirectory(ArtifactsDirectory)
          .SetSymbols(false)
          .SetTargetPath(NuspecFile));
      });

  public Target InstallLocally =>
    _ => _
      .DependsOn(Pack)
      .Executes(() =>
      {
        GlobFiles(ArtifactsDirectory, "*.nupkg")
            .ForEach(x =>
            {
              Dotnet($"new -i {x}");
            });
      });

  public Target Push =>
    _ =>
      _.DependsOn(Pack)
        .OnlyWhenDynamic(() => OnABranchWeWantToPushToNugetOrg())
        .Requires(() => NugetOrgApiKey)
        .Requires(() => GitHasCleanWorkingCopy())
        .Executes(() =>
        {
          GlobFiles(ArtifactsDirectory, "*.nupkg")
            .ForEach(x =>
            {
              this.NugetPushIfVersionDoesNotExistOnNugetOrg(
                x,
                y =>
                {

                  var result = NuGetPush(s => s
                    .SetTargetPath(x)
                    .SetSource("https://www.nuget.org/api/v2/package")
                    .SetApiKey(NugetOrgApiKey)
                    .SetLogOutput(true));

                  var version = this.GetNuGetPackageVersion(x);
                  SendSlackMessage(m => m
                      .SetText(new StringBuilder()
                        .AppendLine($"<!here> :popcorn::shipit: *UnderTest.Templates {version} is out!!!*")
                        .AppendLine()
                        .AppendLine(ChangelogSectionNotes.Select(z => z.Replace("- ", "• ")).JoinNewLine()).ToString()),
                    SlackWebhook);

                  return result;
                });
            });
        });

  // our branching strategy is stable for production, release* for releases and hotfix* for hotfix items.
  public bool OnABranchWeWantToPushToNugetOrg()
  {
     var branch = this.GetCurrentBranch().ToLowerInvariant();
     Logger.Log(LogLevel.Normal, $"Current branch {branch}");

     return branch == "stable"
              || branch.StartsWith("release")
              || branch.StartsWith("hotfix");
  }

  private string GetCurrentBranch()
  {
    var environmentVariable = Environment.GetEnvironmentVariable("APPVEYOR_REPO_BRANCH");
    if (!string.IsNullOrEmpty(environmentVariable))
    {
      return environmentVariable;
    }

    return Git("rev-parse --abbrev-ref HEAD", null, outputFilter: null).Select(x => x.Text).Single();
  }
}
