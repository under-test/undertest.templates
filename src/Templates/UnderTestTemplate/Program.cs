using System.Diagnostics.CodeAnalysis;
using UnderTest;

[assembly:ExcludeFromCodeCoverageAttribute]

namespace UnderTestTemplate
{
  class Program
  {
    static int Main(string[] args)
    {
      return new UnderTestRunner<TestSuiteConfiguration>()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("YOUR PROJECT NAME Test Suite")
          .SetProjectVersion("0.1.0"))
        .WithTestSettings(settings => settings
          .AddAssembly(typeof(Program).Assembly))
        .Execute()
          .ToExitCode();
    }
  }
}
