using UnderTest.Attributes;
using UnderTest;
#if IsSelenium
using UnderTest.Strategy.Selenium;
#endif

namespace MyApp.Namespace
{
  [HandlesFeature("TODOUPDATETHIS.feature")]
#if IsSelenium
  public class FeatureHandlerName : SeleniumFeatureHandler
#else
  public class FeatureHandlerName : FeatureHandler
#endif
  {
    [Given("SOMETHING")]
    public void Given()
    {
    }

    [When("SOMETHING")]
    public void When()
    {
    }

    [Then("SOMETHING")]
    public void Then()
    {
    }
  }
}
