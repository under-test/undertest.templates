# UnderTest.Templates

![UnderTest](https://gitlab.com/under-test/undertest/-/raw/stable/etc/logo.png)
    
[![Active](http://img.shields.io/badge/Status-Active-green.svg)](https://gitlab.com/under-test/undertest) 
[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)](https://gitlab.com/under-test/undertest.templates/blob/stable/LICENSE)
[![NuGet version (UnderTest)](https://img.shields.io/nuget/v/UnderTest.Templates.svg?style=flat-square)](https://www.nuget.org/packages/UnderTest.Templates/)
[![Build status](https://ci.appveyor.com/api/projects/status/r0jhylf1fh0oxupa?svg=true)](https://ci.appveyor.com/project/ThinkingBigRon/undertest-template)
[![NuGet version with pre-releases (UnderTest.Templates)](https://img.shields.io/nuget/vpre/UnderTest.Templates.svg?style=flat-square)](https://www.nuget.org/packages/UnderTest.Template/)

This package contains a set of project and item templates to be used when creating projects from .NET Core dotnet new command line interface for the UnderTest framework.

For more information on UnderTest visit [UnderTest Project](https://gitlab.com/under-test/undertest).

## Getting started

### Requirements

* [.NET Core 2.0 (or newer) SDK](https://dotnet.microsoft.com/).

### Installation

You can install the template by running the following command:

```csharp
dotnet new --install UnderTest.Templates
```

### Updating The Template

You can update the template by running the following command:

```csharp
dotnet new --install UnderTest.Templates
```

This should be done after major releases.  

### Usage

#### UnderTest Project

To create a new UnderTest test suite project, run the following command:

```
dotnet new UnderTest --name "MyProject"
```

This will create a new UnderTest test suite in the current folder.  The following template options are available:

* `-ne|--no-examples` - Add example feature and handler. (Default: true)
* `-twae|--treat-warnings-as-errors` -  Treat warnings as errors. (Default: true)
* `-nr|--no-restore` - Skips the execution of 'dotnet restore' on project creation. (Default: false)

Note: this generated project will need to be added to your solution, if you have one.  This can be done via the CLI by running:

```
dotnet sln .\MyProject.sln add .\TemplateDocsSln.csproj
```
(if your solution does not exist, create it by running `dotnet new sln`)


#### Feature Handler

To add a new UnderTest FeatureHandler to an existing UnderTest project, run the following command:

```
dotnet new undertest-featurehandler HomePageFeatureHandler -n Acme.Project
```

This will create a new UnderTest FeatureHandler in the current folder.  The following template options are available:

* `-is|--is-selenium` - Create selenium handler. (Default: false)
* `-na|--namespace` -  Namespace for the generated code. (Default: `MyApp.Namespace`)

#### Feature

To add a new UnderTest Feature to an existing UnderTest project, run the following command:

```
dotnet new undertest-feature -n HomePage
```

This will create a new UnderTest Feature in the current project.  No template options are available.

#### Feature Service

To add a new UnderTest FeatureService to an existing UnderTest project, run the following command:

```
dotnet new undertest-featureservice -n HomePageFeatureService -na Acme.Project
```

This will create a new UnderTest FeatureService in the current folder.  The following template options are available:

* `-na|--namespace` -  Namespace for the generated code. (Default: `MyApp.Namespace`)

#### StepParam

To add a new UnderTest StepParamAttribute to an existing UnderTest project, run the following command:

```
dotnet new undertest-stepparam YesNoStepParamAttribute -n Acme.Project
```

This will create a new UnderTest StepParamAttribute in the current folder.  The following template options are available:

* `-na|--namespace` -  Namespace for the generated code. (Default: `MyApp.Namespace`)

## Issues Reporting

Happy to help. [Create an issue](https://gitlab.com/under-test/undertest.templates/issues) on this project.

## License

UnderTest.Template is a free open source project, released under the permissible MIT License. This means it can be used in personal and commercial projects for free.  MIT is the same license used by such popular projects as Angular and Dotnet.
