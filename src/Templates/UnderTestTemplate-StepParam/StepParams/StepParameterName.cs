using System;
using UnderTest.Attributes;

namespace MyApp.Namespace
{
  public class StepParameterName : StepParamAttribute
  {
    public override object Transform(object input)
    {
      return input switch
      {
        "TODO" => true,
        "TODO2" => false,
        _ => throw new Exception($"Unknown value in StepParameterName: {input}")
      };
    }
  }
}
