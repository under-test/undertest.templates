using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using NuGet;

using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tooling;

[ExcludeFromCodeCoverage]
public static class BuildExtensions
{
  public static IReadOnlyCollection<Output> NugetPushIfVersionDoesNotExistOnNugetOrg(this Build instance, string filename, Func<string, IReadOnlyCollection<Output>> nugetPushFunc)
  {
    var version = instance.GetNuGetPackageVersion(filename);
    if (instance.NugetPackageVersionExists(instance.GetPackageId(filename), version.ToOriginalString()))
    {
      Logger.Warn("Package not being pushed as it exists on Nuget.org");
      return null;
    }

    return nugetPushFunc(filename);
  }
  
  public static bool NugetPackageVersionExists(this Build instance, string packageIdP, string versionP)
  {
    try
    {
      var url = $"https://api-v2v3search-0.nuget.org/query?q=packageid:{packageIdP}&prerelease=true";

      var response = HttpTasks.HttpDownloadString(url, requestConfigurator: x => x.Timeout = int.MaxValue);
      var packageObject = JsonConvert.DeserializeObject<JObject>(response);

      var versions = packageObject["data"].Single()["versions"].ToArray();

      return versions.ToList().Exists(x => x["version"].ToString() == versionP);
    }
    catch 
    {
      Logger.Warn($"Unable to lookup {packageIdP} on nuget.org for existence.");
      return false;
    }
  }

  public static string GetPackageId(this Build instance, string filename)
  {
    var p = new ZipPackage(filename);

    return p.Id;
  }

  public static SemanticVersion GetNuGetPackageVersion(this Build instance, string filename)
  {
    var p = new ZipPackage(filename);

    return p.Version;
  }
}
