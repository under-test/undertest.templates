using System.Threading.Tasks;
using FluentAssertions;
using JetBrains.Annotations;
using UnderTest;

namespace MyApp.Namespace.Services
{
  [UsedImplicitly]
  public class FeatureServiceName : FeatureHandlerServiceBase
  {
  }
}
