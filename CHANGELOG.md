# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.6.0 - 2020-10-04
### added
- adds configuration approach to the template #34
- adds `ExcludeFromCodeCoverageAttribute` to the assembly #37
### changes
- upgrades to UnderTest 0.8.0
- removes PublicAPI from the handler (now handled by the library) #36
- formatting improvement in program.cs #35

## 0.5.0 - 2020-05-26
### added
- adds Services folder for FeatureServices #24
- adds an item template for FeatureHandler #20
- adds an item template for StepParam #27
- adds an item template for Feature #26
- adds an item template for FeatureService #28
### changes
- changed to use a specific version of UnderTest vs 0.0.0 #18
- refactored to allow for multiple templates #21
- builds are now announced in Slack #23
- updates under test project name #30

## 0.4.0 - 2019-11-12
### added
### changes
- removed `GlobalHandlers` as the feature is deprecated #15
- removed some unneeded `.gitkeep` files #16

## 0.3.0 - 2019-09-02
### added
- added support for the WithCommandLineArgs #6

## 0.2.0 - 2019-08-18
### added
- added support for the ExitCode #6
- adds base dot files #7

## 0.1.2 - 2019-07-02
### changes
- fixes in README

## 0.1.1 - 2019-06-30
### changes
- typo in README

## 0.1.0 - 2019-06-30
### added
- initial version
